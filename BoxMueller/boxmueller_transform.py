# Example code of Box-Muller transform
# for sampling from Gaussian

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm


print("Box-Muller transform for Gaussian sampling")
print("")

number = 100000000
mean = 0
sigma = 10

print("Estimating for N = ", number, " samples")

# random numbers [0,1]
rn1 = np.random.rand(number)
rn2 = np.random.rand(number)

# Box-Muller transform
z = np.sqrt(-2*np.log(rn1))*np.cos(2*np.pi*rn2)
z_final = z * sigma + mean

calc_mean = np.mean(z_final)
calc_std = np.std(z_final)

print("Gaussian distribution with mean = ", calc_mean, " and sigma = ", calc_std)

# calculate variances
print("With variance = ", calc_std**2)
print("And SE = ", calc_std/np.sqrt(number))

# Stuff for plotting a Gaussian line
x = np.linspace(-3*sigma, 3*sigma, 60*sigma)
gauss_line = norm.pdf(x, mean, sigma)
#
plt.figure(0)
plt.suptitle("Gaussian sampling - log scale", fontsize=16)
plt.hist(z_final, 100, density=1, facecolor='green', alpha=0.75)
plt.plot(x, gauss_line, 'k', linewidth=3)
plt.yscale('log', nonpositive='clip')
plt.show(block=True)

plt.figure(1)
plt.suptitle("Gaussian sampling", fontsize=16)
plt.hist(z_final, 100, density=1, facecolor='green', alpha=0.75)
plt.plot(x, gauss_line, 'k', linewidth=3)
plt.show(block=True)
