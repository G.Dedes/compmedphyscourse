# base script for reading mha files

import os
import sys
import SimpleITK as sitk
import matplotlib.pyplot as plt  # for plotting

# function for reading mha files
# returns sitk reader object
import numpy as np


def readIn(fileName):
    # setup the reader
    reader = sitk.ImageFileReader()

    # check if file exists
    if os.path.isfile(fileName):
        # print(fileName)
        reader.SetFileName(fileName)
        return reader
    else:
        print("File ", fileName, " not found!")
        sys.exit("Aborting...")


# main function
# receives file name with full path as input argument
def main():
    print(">>> Executing main ReadMHA <<<\n")
    if len(sys.argv) != 2:
        print("No input file name provided\n")
        sys.exit("Aborting...")

    arg = sys.argv[1]
    print("Input file: ", arg)

    # call read function which returns reader object
    image = readIn(arg)

    # access the header and then print some info
    image.ReadImageInformation()
    print("Image dimensions: ", image.GetDimension())  # get dimension
    print("Image size: ", image.GetSize())  # get image size
    print("Voxel or pixel size: ", image.GetSpacing())  # get voxel or pixel size
    print("")
    # read volume and convert to numpy array
    volume = sitk.GetArrayFromImage(image.Execute())

    totalsumcounts = np.sum(volume)
    print("Total sum of counts: ", totalsumcounts)  # get total sum of counts
    totalareamm = image.GetSize()[0]*image.GetSpacing()[0]*image.GetSize()[1]*image.GetSpacing()[1]
    print("In a total area of: ", totalareamm, " mm2")
    totalcountdensity = totalsumcounts/totalareamm
    print("Total density: ", totalcountdensity, " counts/mm2")
    print("")

    # some exemplary plots
    plt.figure(0)
    plt.gca().set_title("Counts map")
    plt.imshow(volume, cmap=plt.get_cmap('gray'), vmin=0., vmax=100000.)
    plt.xlabel('x in pixels', fontsize=10)  # axis title
    plt.ylabel('y in pixels', fontsize=10)  # axis title
    plt.show(block=False)

    # range in pixels to crop image
    xrange = [190, 210]
    yrange = [90, 110]

    volumecropped = volume[yrange[0]:yrange[1], xrange[0]:xrange[1]]

    croppedsumcounts = np.sum(volumecropped)
    print("Cropped image sum of counts: ", croppedsumcounts)  # get total sum of counts
    croppedareamm = (xrange[1] - xrange[0]) * image.GetSpacing()[0] * (yrange[1] - yrange[0]) * image.GetSpacing()[1]
    print("In a cropped area of: ", croppedareamm, " mm2")
    croppedcountdensity = croppedsumcounts/croppedareamm
    print("Cropped image density: ", croppedcountdensity, " counts/mm2")

    plt.figure(1)
    plt.gca().set_title("Counts map cropped")
    plt.imshow(volumecropped, cmap=plt.get_cmap('gray'), vmin=0., vmax=100000.)
    plt.xlabel('x in pixels', fontsize=10)  # axis title
    plt.ylabel('y in pixels', fontsize=10)  # axis title
    plt.show(block=True)


if __name__ == '__main__':
    main()