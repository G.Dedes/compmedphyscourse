import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft, fft2, fftshift, ifft2

# for plotting
x_plot = np.linspace(0., 1., 100)

# example 1 or 2
example = 2

if example==1:
    # continuous signal, example 1
    # continuous signal components
    dc_plot = np.squeeze(np.full((x_plot.size, 1), 5))
    f1hz_plot = 2*np.cos(2*np.pi*x_plot - np.pi/2.)
    f2hz_plot = 3*np.cos(4*np.pi*x_plot)
    # total signal
    f_plot = dc_plot + f1hz_plot + f2hz_plot
else:
    # continuous signal, example 2
    # continuous signal components
    dc_plot = np.squeeze(np.full((x_plot.size, 1), 6))
    f1hz_plot = np.cos(2*np.pi*x_plot)
    f2hz_plot = 2*np.cos(4*np.pi*x_plot - np.pi)
    f3hz_plot = 5*np.cos(6*np.pi*x_plot - np.pi/2.)
    # total signal
    f_plot = dc_plot + f1hz_plot + f2hz_plot + f3hz_plot

# plot them
print("Plotting signal (black) and its components...")
plt.figure(0)
plt.suptitle("Signal", fontsize=16)
plt.plot(x_plot, f_plot, 'k')
plt.plot(x_plot, dc_plot, 'b')
plt.plot(x_plot, f1hz_plot, 'g')
plt.plot(x_plot, f2hz_plot, 'r')
if example==2:
    plt.plot(x_plot, f3hz_plot, 'y')
# plt.xlabel("y")
# plt.ylabel("x")
# plt.gca().legend(('f', 'g', 'f - g'))
plt.show(block=True)

# sample points
n_samples = 8
sample_points = np.linspace(0., 1., n_samples, endpoint=False)
print("\nSampling at ", n_samples, " Hz")
print("Sampled points: ", sample_points)

# sample the signal at the above points
if example==1:
    # continuous signal, example 1
    # continuous signal components
    dc = np.squeeze(np.full((sample_points.size, 1), 5))
    f1hz = 2 * np.cos(2 * np.pi * sample_points - np.pi / 2.)
    f2hz = 3 * np.cos(4 * np.pi * sample_points)
    # total signal
    f_sampled = dc + f1hz + f2hz
else:
    # continuous signal, example 2
    # continuous signal components
    dc = np.squeeze(np.full((sample_points.size, 1), 6))
    f1hz = np.cos(2 * np.pi * sample_points)
    f2hz = 2 * np.cos(4 * np.pi * sample_points - np.pi)
    f3hz = 5 * np.cos(6 * np.pi * sample_points - np.pi / 2.)
    # total signal
    f_sampled = dc + f1hz + f2hz + f3hz
print("Values of sampled f: ", f_sampled)

# DFT of the samples
f_fft = fft(f_sampled)
print("DFT of sampled f: ", f_fft)
# rounded for example 2
# f_fft = np.array([48.0, 4.0-4.0e-15j, -8.0, -4.0e-15-20j, 7.0e-15, -4.0e-15+20j, -8, 4.0+4.0e-15j])

# signal synthesis
# start with amplitudes
print("\nDoing signal synthesis...")
amplitudes = (1/f_fft.size)*np.sqrt(np.real(f_fft)**2 + np.imag(f_fft)**2)
print("Frequency amplitudes: ", amplitudes)

# calculate phases
phases = np.arctan2(np.imag(f_fft), np.real(f_fft))
print("Frequency phases: ", phases)
