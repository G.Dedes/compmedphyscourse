# base script for reading mha files

import os
import sys
import SimpleITK as sitk
import matplotlib.pyplot as plt  # for plotting

# function for reading mha files
# returns sitk reader object
import numpy as np


def readIn(fileName):
    # set up the reader
    reader = sitk.ImageFileReader()

    # check if file exists
    if os.path.isfile(fileName):
        # print(fileName)
        reader.SetFileName(fileName)
        return reader
    else:
        print("File ", fileName, " not found!")
        sys.exit("Aborting...")


# main function
# receives file name with full path as input argument
def main():
    print(">>> Executing main ReadMHA <<<\n")
    if len(sys.argv) != 2:
        print("No input file name provided\n")
        sys.exit("Aborting...")

    arg = sys.argv[1]
    print("Input file: ", arg)

    # call read function which returns reader object
    image = readIn(arg)

    # access the header and then print some info
    image.ReadImageInformation()
    print("Image dimensions: ", image.GetDimension())  # get dimension
    print("Image size: ", image.GetSize())  # get image size
    print("Voxel or pixel size: ", image.GetSpacing())  # get voxel or pixel size
    print("")
    
    # read volume and convert to numpy array -> this is your CT array
    volume = sitk.GetArrayFromImage(image.Execute())
    volume_crop = volume[200:300,200:300]
    # print(volume[256,:])
    volume_enhanced = np.copy(volume)
    volume_enhanced[volume_enhanced == 1.04] = 4
    volume_enhanced[volume_enhanced == 1.03] = 3
    volume_enhanced[volume_enhanced == 1.] = 0.1

    # Plotting of the CT
    plt.figure(0)
    plt.gca().set_title("Shepp-Logan 2D phantom")
    plt.imshow(volume, cmap=plt.get_cmap('gray'), vmin=1., vmax=1.2)
    plt.xlabel('x in pixels', fontsize=10)  # axis title
    plt.ylabel('y in pixels', fontsize=10)  # axis title
    plt.show(block=True)

    plt.figure(1)
    plt.gca().set_title("Shepp-Logan 2D phantom cropped")
    plt.imshow(volume_crop, cmap=plt.get_cmap('gray'), vmin=1., vmax=1.2)
    plt.xlabel('x in pixels', fontsize=10)  # axis title
    plt.ylabel('y in pixels', fontsize=10)  # axis title
    plt.show(block=True)

    plt.figure(2)
    plt.gca().set_title("Shepp-Logan 2D phantom enhanced contrast")
    plt.imshow(volume_enhanced, cmap=plt.get_cmap('gray'), vmin=0., vmax=4.)
    plt.xlabel('x in pixels', fontsize=10)  # axis title
    plt.ylabel('y in pixels', fontsize=10)  # axis title
    plt.show(block=True)

# Write out modified mha with the same spacing and dimension as the original mha
    out_image = sitk.GetImageFromArray(volume_enhanced)
    sitk.WriteImage(out_image, 'Shepp-Logan_enhanced.mha')

if __name__ == '__main__':
    main()
