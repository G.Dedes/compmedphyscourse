# DFT example
# underlying function is:
# f(t) = 5. + 2*cos(2*pi*t - pi/2) + 3*cos(4*pi*t)
# a DC, an 1 Hz component and a 2 Hz component

import numpy as np
import matplotlib.pyplot as plt

print("Perform DFT using numpy.fft on samples taken from")
print("f(t) = 5. + 2*cos(2*pi*t - pi/2) + 3*cos(4*pi*t)")

# Created a linspace to plot continuous function
t_plot = np.linspace(0, 1, 1000)
dc_plot = 0. * t_plot+5. # just a lazy trick to get 5s
oneHz_plot = 2 * np.cos(2.*np.pi*t_plot - np.pi/2.) # the 1Hz component
twoHz_plot = 3 * np.cos(4.*np.pi*t_plot) # the 2Hz component
f_plot = 5. + 2 * np.cos(2.*np.pi*t_plot - np.pi/2.) + 3 * np.cos(4.*np.pi*t_plot) # the function

# "measure" samples at given times
t_sample = np.arange(0, 1, 0.25)
print("Taking ", np.size(t_sample), " samples at: ", t_sample)

# the function values (my data sample) at the above sampling rate
f_sample = 5. + 2 * np.cos(2.*np.pi*t_sample - np.pi/2.) + 3 * np.cos(4.*np.pi*t_sample)
print("Samples are: ", f_sample)

# take the DFT (FFT) of the sampled signal and print the coefficients
fourier_f = np.fft.fft(f_sample)
print("The Fourier coefficient are: ", fourier_f)

# Plot the function
plt.figure(0)
plt.plot(t_plot, f_plot, color='black')
plt.plot(t_plot, dc_plot, color='blue', linestyle='dashed')
plt.plot(t_plot, oneHz_plot, color='green', linestyle='dashed')
plt.plot(t_plot, twoHz_plot, color='red', linestyle='dashed')
plt.show(block=True)
