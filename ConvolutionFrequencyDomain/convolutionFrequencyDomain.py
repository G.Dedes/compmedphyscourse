import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft2, fftshift, ifft2

########################################################################################################################
# Various images and kernels
# From lecture:
# imageF = np.array([[1., 2., 3.], [4., 5., 6.], [7., 8., 9.]])
# kernelG = np.array([[-1., -2., -1.], [0., 0., 0.], [1., 2., 1.]])
# Other examples
# imageF = np.array([[1., 2., 3., 4., 5.], [6., 7., 8., 9., 10.], [11., 12., 13., 14., 15.],
#                    [16., 17., 18., 19., 20.], [21., 22., 23., 24., 25.]])
# kernelG = np.array([[1., 2., 3., 4., 5.], [6., 7., 8., 9., 10.], [11., 12., 13., 14., 15.],
#                    [16., 17., 18., 19., 20.], [21., 22., 23., 24., 25.]])


########################################################################################################################
# Saturn image
imageF = plt.imread("Saturn.png")
# print(imageF.shape)
# print(imageF.dtype)
imageF = imageF[:, :, 0]
imageF = np.pad(imageF, pad_width=((80, 80), (0, 0)), mode='constant', constant_values=0)
print("Original image size")
print(imageF.shape)

image_fft = fft2(imageF)

########################################################################################################################
# Motion blurring filter
print(">>>>>>> Motion blurring filter")
filter_motionBlur = np.zeros((21,21))
np.fill_diagonal(filter_motionBlur, 1./filter_motionBlur.shape[0])

# Plot filter once before padding
fig = plt.figure()
sizeFont = 14
fig.set_size_inches(15., 7., forward=True)
plt.subplot(2,5,2)
plt.gca().set_title("Motion blurring filter", fontsize=sizeFont)
plt.imshow(filter_motionBlur, cmap=plt.get_cmap('gray'), vmin=0., vmax=0.1)
# plt.show(block=True)
print("Motion blurring filter size before padding")
print(filter_motionBlur.shape)

# Convolution in frequency domain
# Zero padding to match the f + g - 1 rule
filter_motionBlur = np.pad(filter_motionBlur, pad_width=((319, 320), (319, 320)), mode='constant', constant_values=0)
print("Motion blurring filter size after padding")
print(filter_motionBlur.shape)

# Padding also image to the f + g - 1  rule
imageF1 = np.pad(imageF, pad_width=((10, 10), (10, 10)), mode='constant', constant_values=0)
# imageF1 = imageF
print("Image size after padding to match motion blurring filter")
print(imageF1.shape)
image1_fft = fft2(imageF1)

# fft of padded filter
filter_motionBlur_fft = fft2(filter_motionBlur)

# Element-wise multiplication in Fourier space
image_motion_blur_fft = np.multiply(image1_fft, filter_motionBlur_fft)

# Inverse fft
image_motion_blur = fftshift(ifft2(image_motion_blur_fft).real)

########################################################################################################################
# Sharpening filter
print(">>>>>>> Sharpening filter")
filter_sharp = np.array([[-1., -1., -1.], [-1., 9., -1.], [-1., -1., -1.]])

# Plot filter once before padding
plt.subplot(2,5,4)
plt.gca().set_title("Sharpening filter", fontsize=sizeFont)
plt.imshow(filter_sharp, cmap=plt.get_cmap('gray'), vmin=-2., vmax=20.)
# plt.show(block=True)
print("Sharpening filter size before padding")
print(filter_sharp.shape)

# fft of padded filter
# filter_sharp_fft = fft2(filter_sharp)

# Convolution in frequency domain
# Zero padding to match the image
filter_sharp = np.pad(filter_sharp, pad_width=((319, 320), (319, 320)), mode='constant', constant_values=0)
# filter_sharp = np.pad(filter_sharp, pad_width=((600, 600), (600, 600)), mode='constant', constant_values=0)
print("Sharpening filter size after padding")
print(filter_sharp.shape)

# Padding also image to the f + g - 1  rule
imageF2 = np.pad(imageF, pad_width=((1, 1), (1, 1)), mode='constant', constant_values=0)
# imageF2 = imageF
# imageF2 = np.pad(imageF2, pad_width=((600, 600), (600, 600)), mode='constant', constant_values=0)
print("Image size after padding to match sharpening filter")
print(imageF2.shape)
image2_fft = fft2(imageF2)

# fft of padded filter
filter_sharp_fft = fft2(filter_sharp)

# Element-wise multiplication in Fourier space
image_sharp_fft = np.multiply(image2_fft, filter_sharp_fft)

# Inverse fft
image_sharp = fftshift(ifft2(image_sharp_fft).real)

# Crop if extensive zero padding was added
# image_sharp = image_sharp[600:1241, 600:1241]

# Display figures
plt.subplot(2,5,1)
plt.gca().set_title("Original image", fontsize=sizeFont)
plt.imshow(imageF, cmap=plt.get_cmap('gray'), vmin=0., vmax=1.)
# plt.show(block=True)

plt.subplot(2,5,3)
plt.gca().set_title("Motion blurred image", fontsize=sizeFont)
plt.imshow(image_motion_blur, cmap=plt.get_cmap('gray'), vmin=0., vmax=1.)
# plt.show(block=True)

plt.subplot(2,5,5)
plt.gca().set_title("Sharpened image", fontsize=sizeFont)
plt.imshow(image_sharp, cmap=plt.get_cmap('gray'), vmin=0., vmax=1.)
# plt.show(block=True)

plt.subplot(2,5,6)
plt.gca().set_title("FT original image", fontsize=sizeFont)
plt.imshow(fftshift(image_fft.real), cmap=plt.get_cmap('gray'), vmin=0, vmax=0.2)
# plt.show(block=True)

plt.subplot(2,5,7)
plt.gca().set_title("FT motion blurring filter", fontsize=sizeFont)
plt.imshow(fftshift(filter_motionBlur_fft.real), cmap=plt.get_cmap('gray'), vmin=-0.00001, vmax=0.00001)
# plt.imshow(fftshift(filter_motionBlur_fft.real), cmap=plt.get_cmap('gray'))
# plt.show(block=True)

plt.subplot(2,5,8)
plt.gca().set_title("FT motion blurred image", fontsize=sizeFont)
plt.imshow(fftshift(image_motion_blur_fft.real), cmap=plt.get_cmap('gray'), vmin=0., vmax=0.2)
# plt.show(block=True)

plt.subplot(2,5,9)
plt.gca().set_title("FT sharpening filter", fontsize=sizeFont)
plt.imshow(fftshift(filter_sharp_fft.real), cmap=plt.get_cmap('gray'), vmin=-0.00001, vmax=0.00001)
# plt.show(block=True)

plt.subplot(2,5,10)
plt.gca().set_title("FT sharpened image", fontsize=sizeFont)
plt.imshow(fftshift(image_sharp_fft.real), cmap=plt.get_cmap('gray'), vmin=0., vmax=0.2)
plt.show(block=True)
