# Example code of inverse transform
# for sampling from f(x) = 3*x^2

import numpy as np
import matplotlib.pyplot as plt

print("Inverse transform for f(x) = 3*x^2")
print("")

number=100000

print("Estimating for N = ", number, " samples")

# random numbers [0,1]
rn = np.random.rand(number)

# f(x) = 3*x^2
x_plot = np.linspace(0, 1, 100)
f = 3*x_plot**2


# CDF of f is F(x) = x^3
F = x_plot**3
# x = y^(1/3)
x = np.cbrt(rn)

#
plt.figure(0)
plt.suptitle("Sampling from 3*x^2", fontsize=16)
plt.hist(x, bins=100, density=1, facecolor='green', alpha=0.75)
plt.plot(x_plot, f, 'b', linewidth=2)
plt.plot(x_plot, F, 'r', linewidth=2)
plt.grid(color = 'black', linestyle = '--', linewidth = 0.5)
plt.gca().legend(('f(x) = 3*x^2','F(x) = x^3'))
plt.show(block=True)
