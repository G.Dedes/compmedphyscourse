# base script for reading mha files

import os
import sys
import SimpleITK as sitk
import matplotlib.pyplot as plt  # for plotting

# function for reading mha files
# returns sitk reader object
import numpy as np


def readIn(fileName):
    # set up the reader
    reader = sitk.ImageFileReader()

    # check if file exists
    if os.path.isfile(fileName):
        # print(fileName)
        reader.SetFileName(fileName)
        return reader
    else:
        print("File ", fileName, " not found!")
        sys.exit("Aborting...")


# main function
# receives file name with full path as input argument
def main():
    print(">>> Executing main ReadMHA <<<\n")
    if len(sys.argv) != 2:
        print("No input file name provided\n")
        sys.exit("Aborting...")

    arg = sys.argv[1]
    print("Input file: ", arg)

    # call read function which returns reader object
    image = readIn(arg)

    # access the header and then print some info
    image.ReadImageInformation()
    print("Image dimensions: ", image.GetDimension())  # get dimension
    print("Image size: ", image.GetSize())  # get image size
    print("Voxel or pixel size: ", image.GetSpacing())  # get voxel or pixel size
    print("")
    
    # read volume and convert to numpy array -> this is your CT array
    volume = sitk.GetArrayFromImage(image.Execute())

    # Plotting of the CT
    plt.figure(0)
    plt.gca().set_title("Shepp-Logan 2D phantom")
    plt.imshow(volume, cmap=plt.get_cmap('gray'), vmin=1., vmax=1.1)
    plt.xlabel('x in pixels', fontsize=10)  # axis title
    plt.ylabel('y in pixels', fontsize=10)  # axis title
    plt.show(block=True)

if __name__ == '__main__':
    main()
