// Simple code to demonstrate random number generation in C++

// Include headers and libraries
#include <iostream>
#include <cstdlib> // time
#include <chrono>

// calling a specific namespace
using namespace std;

// main class, no other functions
int main() {

    // user defined parameters
    int number = 10; // batch length

    // first print random numbers as they come from uninitialized rand
    cout << "Printing int random numbers before setting any seed:" << endl;
    int randomNint = -1; // initialize variable for int random numbers once
    // loop over length of batch
    for (int i = 0; i < number; ++i) {
        randomNint = rand(); // call a random number from a sequence
        std::cout << randomNint << std::endl; // print it
    }

    // Providing a seed value
    cout << "Printing int random numbers after setting initializing seed with system time:" << endl;
    // srand sets the seed for rand
    srand((unsigned) time(NULL));
    cout << "Timer NULL = " << time(NULL) << endl;
    // loop over length of batch
    for (int i = 0; i < number; ++i) {
        randomNint = rand(); // call a random number from a sequence
        std::cout << randomNint << std::endl; // print it
    }

    // if you want to add delay so that seeds are different if random calls occur within the same second
//    this_thread::sleep_for(std::chrono::milliseconds(5000));

    // transforming random number to a 0->1 uniform sampling
    cout << "Printing float random numbers after setting initializing seed with system time:" << endl;
    // rand returns integers up to a maximum RAND_MAX
    cout << "(RAND_MAX = " << RAND_MAX << ")" << endl;
    cout << "Timer NULL = " << time(NULL) << endl;
    float randomNfloat = -1.0; // initialize variable for float random numbers once
    srand((unsigned) time(NULL)); // for a second time, if we need to draw from a new sequence
    for (int i = 0; i < number; ++i) {
        randomNint = rand(); // call a random number from a sequence
        randomNfloat = randomNint / float(RAND_MAX); // normalize to RAND_MAX
        std::cout << randomNfloat << std::endl; // print it
    }

    return 0;
}
