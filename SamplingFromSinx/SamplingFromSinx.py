# Example of Monte Carlo sampling mean of sin(x)
# Main function is f = sin(x), normalized in 0->pi/2
# F = 1 - cos(x)

import numpy as np
import matplotlib.pyplot as plt

print("Sampling f = sin(x) from 0 to pi/2 with MC")
print("Inverse transform technique")
print()

number = 100000  # number of random number try 10, 100, 1000, 10000
y = np.random.rand(number)

print("Estimating for N = ", number, " trials")
print()

# for plotting figures
x_plot = np.arange(0, np.pi/2., 0.01)
f_plot = np.sin(x_plot)
F_plot = 1. - np.cos(x_plot)

# inverse transform sampling
x = np.arccos(1-y)

# plot figures
plt.figure(0)
# plt.suptitle("Functions", fontsize=16)
plt.hist(x, bins=100, density=1, facecolor='green', alpha=0.75)
plt.plot(x_plot, f_plot, 'b', linewidth=6)
plt.plot(x_plot, F_plot, 'r', linewidth=6)
plt.xlabel("x [rad]")
plt.ylabel("y")
plt.grid(color = 'black', linestyle = '--', linewidth = 0.5)
plt.gca().legend(('f(x) = sin(x)', 'F(x) = 1 - cos(x)'))
plt.show(block=True)

