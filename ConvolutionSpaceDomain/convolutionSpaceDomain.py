import numpy as np
import matplotlib.pyplot as plt
import math
from scipy import signal

# User defined 2D convolution function
def myconvolution2d(myimage, mykernel):
    # get dimensions of image and kernel
    x, y = myimage.shape
    m, n = mykernel.shape
    # create a new empty image filled with zeros
    image_new = np.zeros((x, y))
    # flip kernel in both dimensions
    mykernel = np.flipud(mykernel)
    mykernel = np.fliplr(mykernel)
    # find the middle point index of the kernel
    m_mid = math.floor(m/2)
    n_mid = math.floor(n/2)

    # for each row j in the image
    for j in range(y):
        # depending on j, find the part of the kernel to be used
        # n_min can range from 0 up to the mid point minus the shift
        n_min = max((n_mid - j), 0)
        # n_max can range from the index of the kernel covering the last pixel of the image
        # to n-1 (the last index of the kernel)
        n_max = min((n - 1 - j - n_mid + y - 1), (n-1))
        # depending on j, find the part of the image to be used
        # j_min can range from 0 up to the end of the image minus half the kernel size
        j_min = max((j - n_mid), 0)
        # j_max can range from half the kernel size to the end of the image
        j_max = min((j + n_mid), (y - 1))
        # for each column i in the image
        for i in range(x):
            # Same as above for n and j
            m_min = max((m_mid - i), 0)
            m_max = min((m - 1 - i - m_mid + x - 1), (m-1))
            i_min = max((i - m_mid), 0)
            i_max = min((i + m_mid), (x - 1))
            # According to the indices create the partial image and kernel
            partial_kernel = mykernel[n_min:n_max+1, m_min:m_max+1]
            partial_image = myimage[j_min:j_max+1, i_min:i_max+1]
            # print(partial_kernel.shape)
            # print(partial_image.shape)
            # Multiply element wise the partial image and kernel and sum the result
            image_new[j, i] = np.sum(np.multiply(partial_image, partial_kernel))
    # print("Result from function")
    # print(image_new)
    # print("Function - Library")
    # print(image_new - result)
    # print("Abs sum of difference")
    # print(np.sum(abs(image_new - result)))
    return image_new


# Various images and kernels
# From lecture:
imageF = np.array([[1., 2., 3.], [4., 5., 6.], [7., 8., 9.]])
kernelG = np.array([[-1., -2., -1.], [0., 0., 0.], [1., 2., 1.]])
# Other examples
# imageF = np.array([[1., 2., 3., 4., 5.], [6., 7., 8., 9., 10.], [11., 12., 13., 14., 15.],
#                    [16., 17., 18., 19., 20.], [21., 22., 23., 24., 25.]])
# kernelG = np.array([[1., 2., 3., 4., 5.], [6., 7., 8., 9., 10.], [11., 12., 13., 14., 15.],
#                    [16., 17., 18., 19., 20.], [21., 22., 23., 24., 25.]])

filtered_imageLecture = myconvolution2d(imageF, kernelG)

print("\nImage")
print(imageF)
print("\nKernel")
print(kernelG)
print("\nImage convolved with kernel")
print(filtered_imageLecture)
print("\n")

# Use of convolve2d library
result = signal.convolve2d(imageF, kernelG, mode='same')
print("Result from library")
print(result)

# Saturn image
imageF = plt.imread("Saturn.png")
# print(imageF.shape)
# print(imageF.dtype)
imageF = imageF[:, :, 0]
imageF = np.pad(imageF, pad_width=((80, 80), (0, 0)), mode='constant', constant_values=0)
# print(imageF.shape)

# Motion blurring filter
filter_motionBlur = np.zeros((21,21))
np.fill_diagonal(filter_motionBlur, 1./filter_motionBlur.shape[0])

# Sharpening filter
filter_sharp = np.array([[-1., -1., -1.], [-1., 9., -1.], [-1., -1., -1.]])

# Motion blurring filter
kernelG = filter_motionBlur

# Apply motion blurring via user conv function
image_motionblur = myconvolution2d(imageF, kernelG)

# Sharpening filter
kernelG = filter_sharp

# Apply motion blurring via user conv function
image_sharp = myconvolution2d(imageF, kernelG)

# Display figures
fig = plt.figure()

plt.subplot(1,3,1)
plt.gca().set_title("Original image")
plt.imshow(imageF, cmap=plt.get_cmap('gray'), vmin=0., vmax=1.)
# plt.show(block=True)

plt.subplot(1,3,2)
plt.gca().set_title("Motion blurred image")
plt.imshow(image_motionblur, cmap=plt.get_cmap('gray'), vmin=0., vmax=1.)
# plt.show(block=True)

plt.subplot(1,3,3)
plt.gca().set_title("Sharpened image")
plt.imshow(image_sharp, cmap=plt.get_cmap('gray'), vmin=0., vmax=1.)
plt.show(block=True)

