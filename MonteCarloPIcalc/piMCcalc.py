# Example of Monte Carlo hit-or-miss calculation of pi

# import modules
import numpy as np
import matplotlib.pyplot as plt

# print welcome screen
print("Calculating PI with the MC hit-or-miss method")

number = 10000  # number of random number pairs

# print the size of the array
print("Estimating for N = ", number," pairs")

# random numbers [0,1] in x and y
rn_x = np.random.rand(number)
rn_y = np.random.rand(number)

# print("rn_x = ", rn_x)
# print("rn_y = ", rn_y)

# calculate their radius
radius = np.sqrt(rn_x*rn_x + rn_y*rn_y)

# check for which pairs the radius is larger than 1
# radius_h = radius[radius>1]
# radius_l = radius[radius<=1]
# print(radius)
# print(radius_h)
# print(radius_l)
indices_h = np.nonzero(radius > 1)
indices_l = np.nonzero(radius <= 1)
# print(indices_h)
# print(indices_l)

# for that quarter of the circle, calculate p as:
# pi = 4 * N_in_circle/N_in_square
pi = 4.0 * np.size(indices_l)/number
pi_ref = 3.1415926535  # with 10 decimal digits

# estimate error using history-by-history
# special case for hit-or-miss
I_known = 1.
var = (number/(number-1))*(((I_known)**2 * np.size(indices_h)/number)-((I_known) * np.size(indices_h)/number)**2)
sem = np.sqrt(var/number)

# print what is the deviation from the reference in %
print("PI = ", pi, " diff from ref = ", 100.*(pi - pi_ref)/pi_ref, "% and sem = ", sem)

# calculate circle for display
coord_x = np.arange(0,1,0.001)
coord_y = np.sqrt(np.ones(1000)-coord_x*coord_x)

# plot figures
plt.figure(0)
plt.suptitle("Random number pairs", fontsize=16)
plt.scatter(rn_x[indices_l], rn_y[indices_l], s=1, c='g')
plt.scatter(rn_x[indices_h], rn_y[indices_h], s=1, c='r')
plt.plot(coord_x, coord_y, 'r')
plt.show(block=True)
